# sjsepan-pastelpowerline JSON theme v0.1 for Bash using Oh-My-Posh and NerdFonts on Linux

Customization based on Starship theme sjsepan.pastel-powerline
Edited using VSCode

![sjsepan-pastelpowerline.png](./sjsepan-pastelpowerline.png?raw=true "Screenshot")

## Oh-My-Posh home

<https://ohmyposh.dev/>

## NerdFonts home

<https://www.nerdfonts.com/>

## HOWTO: Install / Configure Oh-My-Posh and standard themes

<https://calebschoepp.com/blog/2021/how-to-setup-oh-my-posh-on-ubuntu/>

## Configure Linux Mint

Copy sjsepan-pastelpowerline.omp.json to ~/.poshthemes/

cd ~
xed .bashrc

Add lines to end:
eval "$(oh-my-posh --init --shell bash --config ~/.poshthemes/sjsepan-pastelpowerline.omp.json)"

## reference for Oh-My-Posh themes

<https://ohmyposh.dev/docs>

## NerdFonts Cheat-Sheet

<https://www.nerdfonts.com/cheat-sheet>

## Customizing theme

Make changes to ~/.poshthemes/sjsepan-pastelpowerline.omp; to test re-execute:
eval "$(oh-my-posh --init --shell bash --config ~/.poshthemes/sjsepan-pastelpowerline.omp.json)"

## History

0.1:
~initial release of theme

Stephen J Sepan
sjsepan@yahoo.com
12-5-2022
